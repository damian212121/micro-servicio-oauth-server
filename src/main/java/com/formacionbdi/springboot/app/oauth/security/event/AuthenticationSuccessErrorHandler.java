package com.formacionbdi.springboot.app.oauth.security.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.formacion.springboot.app.usuarios.commons.models.entity.Usuario;
import com.formacionbdi.springboot.app.oauth.services.IUsuarioService;

import brave.Tracer;
import feign.FeignException;

@Component
public class AuthenticationSuccessErrorHandler implements AuthenticationEventPublisher {

	private Logger log = LoggerFactory.getLogger(AuthenticationSuccessErrorHandler.class);

	@Autowired
	private IUsuarioService usuarioServicio;

	@Autowired
	private Tracer tracer;

	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		UserDetails user = (UserDetails) authentication.getPrincipal();

		String mensaje = "Success Login " + user.getUsername();
		log.info(mensaje);
		Usuario usuario = usuarioServicio.findByUsername(authentication.getName());

		if (usuario.getIntentos() != null && usuario.getIntentos() > 0) {
			usuario.setIntentos(0);
			usuarioServicio.update(usuario, usuario.getId());

		}
	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {

		String mensaje = "ERROR Login " + exception.getMessage();
		log.info(mensaje);
		try {

			StringBuilder errors = new StringBuilder();
			errors.append(mensaje);
			Usuario usuario = usuarioServicio.findByUsername(authentication.getName());
			if (usuario.getIntentos() == null) {
				usuario.setIntentos(0);
			}
			log.info("intentos actual es de :" + usuario.getIntentos());
			usuario.setIntentos(usuario.getIntentos() + 1);
			log.info(" - intentos desoues es de :" + usuario.getIntentos());

			errors.append("intentos del Login: " + usuario.getIntentos());
			if (usuario.getIntentos() >= 3) {
				String error = String.format("El Usuario %s des-habilitado por maximo intentos", usuario.getUsername());
				log.error(error);
				errors.append(" - " + error);
				usuario.setEnabled(false);

			}
			usuarioServicio.update(usuario, usuario.getId());

			tracer.currentSpan().tag("error.mensaje", errors.toString());
		} catch (FeignException e) {
			log.error(String.format("EL Usuario %s no existe en el sistema", authentication.getName()));
		}

	}

}
